package frontend;

import backend.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;



/*
 * Samin Ahmed 
 * 2043024
 */

public class RpsChoice implements EventHandler<ActionEvent> {

    //fields
    private String playerChoice;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private TextField message;
    private RpsGame game;

    public RpsChoice(String playerChoice, TextField wins, TextField losses, TextField ties, TextField message, RpsGame game){
        this.playerChoice = playerChoice;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.message = message;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
        this.message.setText(this.game.playRound(this.playerChoice));
        this.wins.setText("Wins: " +  this.game.getWins());
        this.ties.setText("Ties: " +  this.game.getTies());
        this.losses.setText("Losses: " +  this.game.getLosses());
    }
}
