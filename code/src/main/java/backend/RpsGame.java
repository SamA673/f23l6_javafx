package backend;
import java.util.Random;


/*
 * Samin Ahmed 
 * 2043024
 */
public class RpsGame {
    private int wins;
    private int losses;
    private int ties;   
    private Random random;

    public RpsGame(){
        this.wins = 0;
        this.losses = 0; 
        this.ties = 0; 
        this.random = new Random();
    }


    // Getters
    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getTies() {
        return ties;
    }


    public String playRound(String move){
        if (!(move.equals("rock") || move.equals("scissors") || move.equals("paper")))
            throw new IllegalArgumentException("Expected string 'scissors', 'rock' or 'paper'"); 

        int enemyMove = this.random.nextInt(3); 
        String enemyMoveString = "";
        String computerWinLoss = ""; 

        switch (enemyMove){
            case 0:
                enemyMoveString = "rock";
                if (move.equals("rock")) {
                    this.ties++; 
                    computerWinLoss = "tied";
                } else if(move.equals("paper")){
                    this.wins++;
                    computerWinLoss = "lost"; 
                }else if (move.equals("scissors")){ 
                    this.losses++; 
                    computerWinLoss = "won"; 
                }
                break; 
            case 1: 
                enemyMoveString = "scissors";

                if (move.equals("rock")) {
                    this.wins++; 
                    computerWinLoss = "lost";
                }else if(move.equals("paper")) {
                    this.losses++;
                    computerWinLoss = "won";
                }else if (move.equals("scissors")) {
                    this.ties++;
                    computerWinLoss = "tied"; 
                }
                break;
            case 2: 
                enemyMoveString = "paper"; 
                if (move.equals("rock")) {
                    this.losses++; 
                    computerWinLoss = "won";
                }else if(move.equals("paper")){
                     this.ties++;
                     computerWinLoss = "tied";
                }else if (move.equals("scissors")) {
                    this.wins++;
                    computerWinLoss = "lost"; 
                }
                break;
        }

        return "Computer played " + enemyMoveString + " and " + computerWinLoss; 
    }
}
