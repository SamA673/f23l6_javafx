package rpsgame;
import java.util.Scanner;
import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */


 /*
 * Samin Ahmed 
 * 2043024
 */
public class App 
{
    public static void main( String[] args )
    {
        boolean isDone = false;
        Scanner scan = new Scanner(System.in); 
        RpsGame game = new RpsGame();

        while(!isDone){
            try{
                System.out.println("Enter your move... (scissors, rock or paper) or 0 to exit");
                String move = scan.nextLine();

                if(move.equals("0")) {
                    isDone = true; 
                } else {
                    String result = game.playRound(move);
                    System.out.println(result);
                }

            }catch(IllegalArgumentException e){
                System.out.println("Please enter 'scissors', 'rock' or paper");
            }
        }

        System.out.println("Total wins: " + game.getWins()); 
        System.out.println("Total ties: " + game.getTies()); 
        System.out.println("Total losses: " + game.getLosses()); 
    }
}
